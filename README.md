### What is this repository for? ###
Slip model for the 1907 Sumatra earthquake from Martin et al (2019)

### Recommended Citation ###
Martin, S.S., L. Li, E.A. Okal, J. Morin, A.E.G. Tetteroo, A.D. Switzer, and K.E. Sieh (2019), Reassessment of the 1907 Sumatra "Tsunami Earthquake" Based on Macroseismic, Seismological, and Tsunami Observations, and Modeling, Pure Appl. Geophys., 176(7), 2831–2868, https://doi.org/10.1007/s00024-019-02134-2

### Who do I talk to? ###

Contact Stacey Martin (7point1@gmail.com) or Linlin Li (lilinlin3@mail.sysu.edu.cn)